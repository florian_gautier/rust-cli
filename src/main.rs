use std::fs::File;

use std::error::Error;
use std::io::BufReader;
use serde::{Deserialize, Serialize};
use structopt::StructOpt;


#[derive(Serialize, Deserialize, Debug)]
struct Bom {
    param: String
}

/// A cli to manage bom files
# [derive(Debug,StructOpt)]
struct App {

    /// Location of the bom file
    #[structopt(long)]
    bom: String
}
fn main() -> Result<(), Box<dyn Error>>{

    let app = App::from_args();


    let bom_file = File::open(app.bom)?;
    let reader = BufReader::new(bom_file);

    let bom: Bom = serde_json::from_reader(reader)?;

    println!("Parsed bom {:?}", bom);

    println!("Param value {}", bom.param);

    Ok(())
}
